defmodule Mining do
  def mine(name, nonce) do
    hash = :crypto.hash(:sha256, "#{name}#{nonce}")
    |> Base.encode16
    |> String.downcase

    if Regex.match?(~r/^0000/, hash) do
      nonce
    else
      mine(name, nonce + 1)
    end

  end
end

defmodule Blockchain do
  @moduledoc """
  Documentation for Blockchain.
  """

  @doc """
  """
  def hello do
    Mining.mine("Bruna", 0)
    |> IO.puts
  end
end

Blockchain.hello()
